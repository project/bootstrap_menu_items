<?php

/**
 * @file
 * Drupal Module: Bootstrap menu items.
 *
 * @author: Alexander Hass <https://drupal.org/user/85918>
 */

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Template\Attribute;

/**
 * Implements hook_entity_type_build().
 */
/*
function bootstrap_menu_items_entity_type_build(array &$entity_types) {
  if (isset($entity_types['menu_link_content'])) {
    $entity_types['menu_link_content']->set('originalClass', 'Drupal\menu_link_content\Entity\MenuLinkContent');
    $entity_types['menu_link_content']->setClass('Drupal\bootstrap_menu_items\MenuLinkContent');
  }
} */

/**
 * Implements hook_entity_base_field_info_alter().
 */
/*
function bootstrap_menu_items_entity_base_field_info_alter(&$fields, EntityTypeInterface $entity_type) {
  if ($entity_type->id() == 'menu_link_content') {
    $fields['link']->setRequired(FALSE);
  }
}
*/

/**
 * Implements hook_preprocess_HOOK().
 */
function bootstrap_menu_items_preprocess_table__menu_overview(&$variables) {
  foreach ($variables['rows'] as &$row) {
    if ($url = $row['cells'][0]['content'][1]['#url']) {
      if ($url->isRouted() && !$url->getRouteName()) {
        $title = $row['cells'][0]['content'][1]['#title'];
        $row['cells'][0]['content'][1] = [
          '#markup' => $title
        ];
      }
    }
  }
}

/**
 * Implementation of hook_preprocess_HOOK().
 */
function bootstrap_menu_items_preprocess_menu(&$variables) {
  foreach ($variables['items'] as $menu_link_key => &$menu_link) {
    bootstrap_menu_items_preprocess_menu_link($menu_link);
  }
}

/**
 * Recursive menu_link item fixer.
 */
function bootstrap_menu_items_preprocess_menu_link(&$menu_link) {
  if ($menu_link['url']->isRouted() && !$menu_link['url']->getRouteName()) {
    $menu_link['no_link'] = TRUE;

    // Copy the old attributes, not to happy about this, because our module get's called before the theme layer.
    $attributes = $menu_link['url']->getOption('attributes');
    $attributes['class'][] = 'nolink';

    // Create a new attribute object to sent to the twig layer.
    $menu_link['no_link_attributes'] = new Attribute($attributes);
  }

  if (isset($menu_link['below']) && count($menu_link['below'])) {
    foreach ($menu_link['below'] as &$child_menu_link) {
      bootstrap_menu_items_preprocess_menu_link($child_menu_link);
    }
  }
}

/**
 * Implements hook_form_FROM_ID_alter().
 */
function bootstrap_menu_items_menu_link_content_menu_link_content_form_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {
  // Some menu items have a pre-defined path which cannot be modified hence no
  // default_value.
  if (isset($form['link']['widget'][0]['uri']['#default_value'])) {
    $default_value = $form['link']['widget'][0]['uri']['#default_value'];

    if (preg_match('/^<nolink>\/[0-9]+$/', $default_value)) {
      $default_value = 'route:<nolink>';
    }
    elseif (preg_match('/^<header>\/[0-9]+$/', $default_value)) {
      $default_value = 'route:<header>';
    }
    elseif (preg_match('/^<separator>\/[0-9]+$/', $default_value)) {
      $default_value = 'route:<separator>';
    }

    $form['link']['widget'][0]['uri']['#default_value'] = $default_value;
    $form['link']['widget'][0]['uri']['#description'][] = t('Enter "%nolink" to generate non-linkable menu item, enter "%header" to generate a header item and "%separator" to generate a divider item.', array('%nolink' => 'route:<nolink>', '%header' => 'route:<header>', '%separator' => 'route:<separator>'));
  }
}
