Module: Bootstrap menu items
Author: Alexander Hass <https://drupal.org/user/85918>

Description
===========
Allow you to set <header> and <separator> as paths in menus. The menu item name
is than used as Header name. This implements known Bootstrap 3 Header and
Divider in dropdown navigations like you can see at
https://getbootstrap.com/components/#dropdowns-headers. 

Drupal 8 supports <nolink> out of the box. Enter "route:<nolink>" as menu route.


Requirements
============

* Drupal Core - Menu module
